# Gerrit to MQTT

Goal of this app is to listen to gerrit stream event of a gerrit server and send
them into MQTT topics.

each project of the server will have a master topic and subtopics following this
rule:

```bash
master_topic
├── project_one
│   ├── comment-added
│   ├── ...
│   └── patchset-created
├── ...
│
└── project_X
    ├── comment-added
    ├── ...
    └── patchset-created
```

## Configuration

configuration is done via a configuration file
an example is in etc/gerrit-to-mqtt.conf:

```gitconfig
[gerrit]
username=gerrit_username
hostname=localhost
key=~/.ssh/id_rsa
# if port not set, default to 29418
port=29418
# set proxy only if needed
# socks proxy only is supported today
proxy_type=socks
proxy_host=host
proxy_port=port

[mqtt]
hostname=localhost
topic=gerrit
# If qos is not set, default to 0
qos=2
# If port is not set, default to 1883
port=1883
# If you need authentication on the MQTT Broker
username=username
password=password
```

## Usage

### Command Line

After installation of the requirements, launch the script with the path of the
configuration file as arguments

```bash
pip install -r requirements.txt
python gerrit-to-mqtt.py etc/gerrit-to-mqtt.conf
```

### Docker

you'll have to put the right path of the id_rsa key in `gerrit-to-mqtt.conf`. In
the example below, it would be `key=/etc/ssh/id_rsa`.

```bash
docker run  --volume path/to/configuration:/etc/gerrit-to-mqtt.conf:ro \
            --volume path/to/id_rsa:/etc/ssh/id_rsa:ro \
    registry.gitlab.com/orange-opensource/lfn/ci_cd/gerrit-to-mqtt:latest
```

## TODO

- [ ] put log directly on output
- [ ] choose log level in the configuration and via the command line
- [ ] allow the use of websockets
