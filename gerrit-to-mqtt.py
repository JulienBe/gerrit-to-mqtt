#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import argparse
import json
import logging
import logging.config
import pkg_resources
import os
from threading import Event
import time

from pygerrites.client import GerritClient
from pygerrites.error import GerritError
from pygerrites.events import ErrorEvent

import paho.mqtt.publish as publish
from six.moves import configparser as ConfigParser

logging.config.fileConfig(pkg_resources.resource_filename('gerrit-to-mqtt','logging.ini'))
logging.captureWarnings(True)
logger = logging.getLogger("gerrit_to_mqtt")


def _get_options():
    parser = argparse.ArgumentParser(
        description="client that publish a gerrit event stream on MQTT")
    parser.add_argument('conffile', nargs=1, help="Configuration file")
    return parser.parse_args()

class GerritStream(object):

    def __init__(self, username, host, key=None, proxy_type=None,
                 keepalive=60, proxy_host=None, proxy_port=None,
                 thread=True, port=29418):
        logger.debug("creating a gerrit client with current values:")
        logger.debug("  * username: %s", username)
        logger.debug("  * host: %s", host)
        logger.debug("  * key_filename: %s", key)
        logger.debug("  * port: %s", port)
        self.gerrit = GerritClient(
            username=username,
            host=host,
            key_filename=key,
            port=port,
            keepalive=keepalive,
            proxy_type=proxy_type,
            proxy_host=proxy_host,
            proxy_port=proxy_port)
        infos = self.gerrit.gerrit_info()
        logger.info("connected to gerrit version %s with user %s",
                    infos[1], infos[0])
        if thread:
            self.gerrit.start_event_stream()

    def get_event(self):
        return self.gerrit.get_event()


class PushMQTT(object):
    def __init__(self, hostname, port=1883, client_id=None,
                 keepalive=60, will=None, auth=None, tls=None, qos=0):
        logger.debug("creating a mqqt client with current values:")
        logger.debug("  * hostname: %s", hostname)
        logger.debug("  * client_id: %s", client_id)
        logger.debug("  * port: %s", port)
        logger.debug("  * keepalive: %s", keepalive)
        logger.debug("  * qos: %s", qos)
        logger.debug("  * auth: %s", auth)
        logger.debug("  * tls: %s", tls)
        logger.debug("  * will: %s", will)

        self.hostname = hostname
        self.port = port
        self.client_id = client_id
        self.keepalive = 60
        self.will = will
        self.auth = auth
        self.tls = tls
        self.qos = qos

    def publish_single(self, topic, msg):
        publish.single(topic, msg, hostname=self.hostname,
                       port=self.port, client_id=self.client_id,
                       keepalive=self.keepalive, will=self.will,
                       auth=self.auth, tls=self.tls, qos=self.qos)

def get_topic(base_topic, event):
    event_type = event.name
    if hasattr(event, 'change'):
        project = event.change.project
        if not project and hasattr(event, 'ref_update'):
            project = event.ref_update.project
        pieces = [base_topic, project, event_type]
    elif hasattr(event, 'ref_update'):
        project = event.ref_update.project
        pieces = [base_topic, project, event_type]
    else:
        pieces = [base_topic, event_type]
    return "/".join(pieces)

def main():
    args = _get_options()
    config = ConfigParser.ConfigParser()
    config.read(args.conffile)

    # Gerrit Configuration
    port = config.get('gerrit', 'port', fallback=29418)
    key = config.get('gerrit', 'key', fallback=None)
    proxy_type = config.get('gerrit', 'proxy_type', fallback=None)
    proxy_host = config.get('gerrit', 'proxy_host', fallback=None)
    proxy_port = config.getint('gerrit', 'proxy_port', fallback=None)
    ssh_keepalive = config.getint('gerrit', 'keepalive', fallback=60)
    if proxy_type and proxy_type != "socks":
        raise ValueError("Only proxy_type 'socks' is allowed")
    logger.debug("gerrit client creation")
    gerrit = GerritStream(
        config.get('gerrit', 'username'),
        config.get('gerrit', 'hostname'),
        key=key,
        port=port,
        keepalive=ssh_keepalive,
        proxy_type=proxy_type,
        proxy_host=proxy_host,
        proxy_port=proxy_port)


    # MQTT Client Configuration
    mqtt_port = config.getint('mqtt', 'port', fallback=1883)
    mqtt_keepalive = config.getint('mqtt', 'keepalive', fallback=60)

    # Configure auth
    auth = None
    mqtt_username = config.get('mqtt', 'username', fallback=None)
    mqtt_password = config.get('mqtt', 'password', fallback=None)
    if mqtt_username:
        auth = {'username': mqtt_username}
        if mqtt_password:
            auth['password'] = mqtt_password

    # QOS setting
    mqtt_qos = config.getint('mqtt', 'qos', fallback=0)

    logger.debug("MQTT client creation")
    mqttqueue = PushMQTT(
        config.get('mqtt', 'hostname'),
        port=mqtt_port,
        keepalive=mqtt_keepalive,
        auth=auth,
        qos=mqtt_qos)

    base_topic = config.get('mqtt', 'topic')
    while True:
        event = gerrit.get_event()
        logger.debug("event of type %s received", event.name)
        topic = get_topic(base_topic, event)
        logger.debug("topic: %s", topic)
        logger.debug("event: %s", json.dumps(event.json))

        if event:
            if event.name == "error-event":
                exit(1)
            else:
                mqttqueue.publish_single(topic, json.dumps(event.json))

if __name__ == "__main__":
    main()
