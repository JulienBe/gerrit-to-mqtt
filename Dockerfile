FROM alpine:3.8

ARG PIP_TAG=18.0

WORKDIR /opt/gerrit-to-mqtt

COPY requirements.txt requirements.txt

RUN apk add --no-cache libressl\
                       openssh-client \
                       py3-pip \
                       python3 &&\
    apk add --no-cache --virtual .build-deps build-base \
                                             git \
                                             libffi-dev \
                                             libressl-dev \
                                             python3-dev &&\
    pip3 install --no-cache-dir --upgrade pip==$PIP_TAG && \
    pip3 install --no-cache-dir -r requirements.txt &&\
    apk del .build-deps

COPY . .

ENTRYPOINT ["python3", "gerrit-to-mqtt.py"]
CMD ["/etc/gerrit-to-mqtt.conf"]
